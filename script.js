let body = document.getElementById("body");
let ville = document.getElementById("ville");
let btnSearch = document.getElementById("btnSearch");
let boxAlert = document.getElementById("boxAlert");
let spanVilleCible = document.getElementById("spanVilleCible");
let temperature = document.getElementById("temperature");
let description = document.getElementById("description");
let humidity = document.getElementById("humidity");
let windSpeed = document.getElementById("windSpeed");


ville.addEventListener("keyup", function (event) {
    if (event.key === "Enter") {
        inputValidation();
        ville.value = "";
    }
})

btnSearch.addEventListener("click", function (event) {
    inputValidation();
    ville.value = "";
})


function inputValidation() {
    if (ville.value === "") {
        boxAlert.classList.remove("d-none");
    }
    else {
        boxAlert.classList.add("d-none");

        axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${ville.value}&units=metric&appid=f1d14debaa874f9e1698ca874b570186`)
            .then(response => {
                spanVilleCible.textContent = `Weather in ${response.data.name}`
                temperature.textContent = `${response.data.main.temp}°C`
                let temp = response.data.weather;
                description.innerHTML = `<img src="https://openweathermap.org/img/wn/${temp[0].icon}.png">${temp[0].description}`
                humidity.textContent = `Humidity : ${response.data.main.humidity}%`
                windSpeed.textContent = `Wind speed : ${response.data.wind.speed} km/h`
                body.style.backgroundImage = `url(https://source.unsplash.com/1600x900/?${response.data.name})`
            });
    }
}
